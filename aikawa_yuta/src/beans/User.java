package beans;

import java.io.Serializable;

public class User implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String login_id;
	private String password;
	private int branch_id;
	private int section_id;
	private int is_deleted;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getBranch_id(){
		return branch_id;
	}

	public void setBranch_id(int branch_id){
		this.branch_id = branch_id;
	}

	public int getSection_id() {
		return section_id;
	}

	public void setSection_id(int section_id) {
		this.section_id = section_id;
	}

	public int getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}

}

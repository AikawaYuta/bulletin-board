package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Section;
import dao.SectionDao;;


public class SectionService {
	public List<Section> getSections(){

		Connection connection = null;
		try {
			connection = getConnection();

			SectionDao SectionDao = new SectionDao();
			List<Section> ret = SectionDao.getSections(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}


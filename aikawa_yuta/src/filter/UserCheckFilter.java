package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/user_controll","/signup","/edit_user"})
public class UserCheckFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
        // セッションが存在しない場合NULLを返す
		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		HttpSession session = ((HttpServletRequest) request).getSession();
		int section_id = user.getSection_id();
        if(section_id == 1){
            // セッションが本社総務であれば、通常どおりの遷移

        	chain.doFilter(request, response);
        }else{
            // それ以外ならば、弾く
        	String messages = "アクセス権限がありません";
			session.setAttribute("errorMessages",messages );
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}
	}


	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy() {
	}

}

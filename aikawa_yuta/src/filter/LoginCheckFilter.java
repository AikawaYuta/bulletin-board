package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginCheckFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {


        // セッションが存在しない場合NULLを返す

		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		HttpSession session = ((HttpServletRequest) request).getSession();

		String requestPath = ((HttpServletRequest)request).getServletPath();
		String messages = "ログインしてください";

		if(requestPath.equals("/login") == false && !requestPath.matches("/css/.*")){
	        if(user == null){
	            // セッションがNULLならば、ログイン画面へ飛ばす
				session.setAttribute("errorMessages",messages );
				((HttpServletResponse) response).sendRedirect("login");
				return;
	        }
	        //System.out.println(messages);
		}
		chain.doFilter(request, response);
	}


	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy() {
	}

}
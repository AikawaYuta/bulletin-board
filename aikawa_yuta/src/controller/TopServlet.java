package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;
import service.UserService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	//メッセージ表示
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		User user = (User) request.getSession().getAttribute("loginUser");
		int login_user = user.getId();
		request.setAttribute("login_user", login_user);

	//検索条件の値の保持
		String startDay = request.getParameter("startDay");
		request.setAttribute("start", startDay);
		String endDay = request.getParameter("endDay");
		request.setAttribute("end", endDay);
		String category = request.getParameter("category");
		request.setAttribute("cateGory", category);

		if (StringUtils.isBlank(startDay)){
			startDay = "2017/11/30";
		}
		if (StringUtils.isBlank(endDay)){
			endDay = "2017/12/31";
		}
		if (StringUtils.isBlank(category)){
			category = null;
		}
		List<UserMessage> messages = new MessageService().getMessage(startDay, endDay,category);
		request.setAttribute("messages", messages);

		//削除機能の表示に使う
		//request.setAttribute("isShowMessageForm", isShowMessageForm);

		List<UserComment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		//全ユーザー情報の取得
		List<User> users = new UserService().users();
		request.setAttribute("users", users);


		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

//コメントの投稿
		List<String> comments = new ArrayList<String>();
		if (isValid(request, comments) == true) {

			User user = (User) request.getSession().getAttribute("loginUser");
			Comment comment = new Comment();
			comment.setUser_id(user.getId());
			String new_id = request.getParameter("message_id");
			int message_id = Integer.parseInt(new_id);
			comment.setMessage_id(message_id);
			comment.setText(request.getParameter("comment_text"));


			new CommentService().register(comment);

			response.sendRedirect("./");
			return;
		} else {
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
			return;
		}
	}

	//コメント条件
	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("comment_text");

		if (StringUtils.isBlank(comment) == true) {
			comments.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

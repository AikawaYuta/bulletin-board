package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.MessageService;

@WebServlet(urlPatterns = { "/deletemessage" })
public class DeleteMessageServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

	//投稿削除機能
			String userid = request.getParameter("message_id");
			int delete_id = Integer.parseInt(userid);
			Message delete = new Message();
			delete.setId(delete_id);
			new MessageService().deleteUserMessage(delete);
			response.sendRedirect("./");
	}
}

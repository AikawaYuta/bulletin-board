package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;



@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}


		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		Message newMessage = new Message();
		newMessage.setTitle(request.getParameter("title"));
		newMessage.setCategory(request.getParameter("category"));
		newMessage.setText(request.getParameter("text"));

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			newMessage.setUser_id(user.getId());

			new MessageService().register(newMessage);

			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("messages", newMessage);
			request.getRequestDispatcher("message.jsp").forward(request, response);


		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isBlank(title) == true){
			messages.add("タイトルを入力してください");
		}
		if (30 < title.length()) {
			messages.add("30文字以下でタイトルを入力してください");
		}
		if(StringUtils.isBlank(category) == true){
			messages.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			messages.add("10文字以下でカテゴリーを入力してください");
		}

		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < text.length()) {
			messages.add("1000文字以下で本文を入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

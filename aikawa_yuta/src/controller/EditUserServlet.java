package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Section;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.SectionService;
import service.UserService;


@WebServlet(urlPatterns = { "/edit_user" })
public class EditUserServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();



		//セレクトボックス用
		//支店情報
		List<Branch> getBranches =new BranchService().getBranches();
		request.setAttribute("branches", getBranches);
		//部門情報
		List<Section> getSections = new SectionService().getSections();
		request.setAttribute("sections", getSections);


		//パスワードを更新しない際の処理
		String password = request.getParameter("password");
		if (StringUtils.isEmpty(password)){
			password = null;
		}

		//ID指定の例外処理
		if(StringUtils.isBlank(request.getParameter("id")) == true){
			String messages = "IDを指定してください";
			session.setAttribute("errorMessages",messages );
			response.sendRedirect("user_controll");
			return;
		}
		if(request.getParameter("id").matches("\\D*")){
			String messages = "不正なIDが指定されました";
			session.setAttribute("errorMessages",messages );
			response.sendRedirect("user_controll");
			return;
		}
		String userid = request.getParameter("id");
		int id = Integer.parseInt(userid);
		UserService UserService = new UserService();
		User user = UserService.editUser(id);
		session.setAttribute("editUser",user);

		if(user ==null){
			String messages = "IDを指定してください";
			session.setAttribute("errorMessages",messages );
			response.sendRedirect("user_controll");
			return;
		}

		else {
			request.getRequestDispatcher("edit_user.jsp").forward(request, response);
		}

	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		User user = getEditUser(request);
		String userid = request.getParameter("id");
		int id = Integer.parseInt(userid);
		String password = request.getParameter("password");
		if (StringUtils.isEmpty(password)){
			password = null;
		}

		//支店情報
		List<Branch> getBranches =new BranchService().getBranches();
		request.setAttribute("branches", getBranches);
		//部門情報
		List<Section> getSections = new SectionService().getSections();
		request.setAttribute("sections", getSections);

		session.setAttribute("editUser",user);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(user,id,password);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("edit_user");
			}

			session.setAttribute("editUser", user);
			response.sendRedirect("user_controll");
		} else {
			request.setAttribute("editUser", user);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("edit_user.jsp").forward(request, response);
		}
	}
	//編集対象ユーザーの情報取得
	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		User editUser = (User) session.getAttribute("editUser");

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		editUser.setSection_id(Integer.parseInt(request.getParameter("section_id")));
		editUser.setPassword(request.getParameter("password"));
		return editUser;
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String branch_id = request.getParameter("branch_id");
		String section_id = request.getParameter("section_id");
		String password = request.getParameter("password");
		String secondPassword = request.getParameter("secondPassword");
		UserService UserService = new UserService();
		User user = UserService.getLogin_id(login_id);
		String id = request.getParameter("id");
		int old_user = Integer.parseInt(id);
		int new_user = user.getId();



		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true){
			messages.add("ログインIDを入力してください");
		}
		if(!user.equals(null) && old_user != new_user ){
			messages.add("すでに登録されたログインIDです");
		}
		if (!password.equals(secondPassword)){
			messages.add("パスワードが一致しません");
		}
		if (StringUtils.isEmpty(branch_id) == true){
			messages.add("支店を入力してください");
		}
		if (StringUtils.isEmpty(section_id) == true){
			messages.add("部署を入力してください");
		}
		else if(branch_id.equals("1") &&( section_id.equals("3") || section_id.equals("4"))){
			messages.add("支店と部署の組み合わせが不正です");
		}
		else if((section_id.equals("1") || section_id.equals("2")) && !(branch_id.equals("1"))){
			messages.add("支店と部署の組み合わせが不正です");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}


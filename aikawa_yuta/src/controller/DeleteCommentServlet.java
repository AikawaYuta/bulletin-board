package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.CommentService;

@WebServlet(urlPatterns = { "/deletecomment" })
public class DeleteCommentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

	//コメント削除機能
			String comid = request.getParameter("comment_id");
			int comment_id = Integer.parseInt(comid);
			Comment delete = new Comment();
			delete.setId(comment_id);
			new CommentService().deleteUserComment(delete);
			response.sendRedirect("./");
	}
}


package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Section;
import beans.User;
import service.BranchService;
import service.SectionService;
import service.UserService;

@WebServlet(urlPatterns = { "/user_controll" })

public class UserControllServlet extends HttpServlet  {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isUserList;
		if (user != null) {
			isUserList = true;
		} else {
			isUserList = false;
		}
		//ユーザー情報
		List<User> users = new UserService().users();
		request.setAttribute("users", users);
		request.setAttribute("isUserList", isUserList);
		//支店情報
		List<Branch> getBranches =new BranchService().getBranches();
		request.setAttribute("branches", getBranches);
		//部門情報
		List<Section> getSections = new SectionService().getSections();
		request.setAttribute("sections", getSections);


		String id = request.getParameter("id");
		request.setAttribute("id", id);

		request.getRequestDispatcher("user_controll.jsp").forward(request, response);


//停止復活
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String delete = request.getParameter("is_deleted");
		int is_deleted = Integer.parseInt(delete);

		String userid = request.getParameter("id");
		int id = Integer.parseInt(userid);


		User updateUser = new User();
		updateUser.setId(id);
		updateUser.setIs_deleted(is_deleted);



		new UserService().updateUser(updateUser, id);
		response.sendRedirect("user_controll");
	}
}

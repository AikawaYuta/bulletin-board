package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Section;
import beans.User;
import service.BranchService;
import service.SectionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//支店情報
		List<Branch> getBranches =new BranchService().getBranches();
		request.setAttribute("branches", getBranches);
		//部門情報
		List<Section> getSections = new SectionService().getSections();
		request.setAttribute("sections", getSections);


		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		List<String> message = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);
		//支店情報
		List<Branch> getBranches =new BranchService().getBranches();
		request.setAttribute("branches", getBranches);
		//部門情報
		List<Section> getSections = new SectionService().getSections();
		request.setAttribute("sections", getSections);

		if (isValid(request, message) == true) {

			new UserService().register(signUpUser);

			response.sendRedirect("user_controll");
		} else {
			request.setAttribute("signUpUser", signUpUser);
			request.setAttribute("errorMessages", message);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private User getSignUpUser(HttpServletRequest request)
			throws IOException, ServletException {

		User signUpUser = new User();
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setLogin_id(request.getParameter("login_id"));
		signUpUser.setPassword(request.getParameter("password"));
		String branch = request.getParameter("branch_id");
		int branch_id = Integer.parseInt(branch);
		signUpUser.setBranch_id(branch_id);
		String section = request.getParameter("section_id");
		int section_id = Integer.parseInt(section);
		signUpUser.setSection_id(section_id);
		return signUpUser;
	}




	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String login_id = request.getParameter("login_id");
		String branch = request.getParameter("branch_id");
		int branch_id = Integer.parseInt(branch);
		String section = request.getParameter("section_id");
		int section_id = Integer.parseInt(section);
		String secondPassword = request.getParameter("secondPassword");
		UserService UserService = new UserService();
		User user = UserService.getLogin_id(login_id);


		if (StringUtils.isBlank(login_id) == true){
			messages.add("ログインIDを入力してください");

		}else if (6 > login_id.length()) {
			messages.add("6文字以上でログインIDを入力してください");
		}
		else if (20 < login_id.length()) {
			messages.add("20文字以下でログインIDを入力してください");
		}
		if(user != null){
			messages.add("すでに登録されたログインIDです");
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		if (10 < name.length()) {
			messages.add("10文字以下で名前を入力してください");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}
		else if (6 > password.length()) {
			messages.add("6文字以上でパスワードを入力してください");
		}
		else if (20 < password.length()) {
			messages.add("20文字以下でパスワードを入力してください");
		}
		if (!password.equals(secondPassword)){
			messages.add("パスワードが一致しません");
		}

		if (branch_id == 0){
			messages.add("支店を選択してください");
		}
		if (section_id ==0){
			messages.add("部署を選択してください");
		}
		else if(branch_id == 1 &&( section_id == 3 || section_id == 4)){
			messages.add("支店と部署の組み合わせが不正です");
		}
		else if((section_id == 1 || section_id == 2) && !(branch_id == 1) ){
			messages.add("支店と部署の組み合わせが不正です");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

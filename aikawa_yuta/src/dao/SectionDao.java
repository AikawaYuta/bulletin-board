package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Section;
import exception.SQLRuntimeException;

public class SectionDao {
	public List<Section> getSections(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM SECTIONS");


			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Section> ret = toSectionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Section> toSectionList(ResultSet rs)
			throws SQLException {

		List<Section> ret = new ArrayList<Section>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Section sections = new Section();
				sections.setId(id);
				sections.setName(name);


				ret.add(sections);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


}

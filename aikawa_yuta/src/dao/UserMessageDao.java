package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {
	public List<UserMessage> getUserMessages(Connection connection, int num, String start, String end,String category) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM messages");
			sql.append(" where");
			sql.append(" insert_date >= ?");
			sql.append(" AND");
			sql.append(" insert_date < ?");
			if (category != null){
				sql.append(" AND");
				sql.append(" category LIKE ? ");
			}
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, start);
			ps.setString(2, end + "/23:59:59");
			if(category != null){
				ps.setString(3,"%" + category + "%" );
			}

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title =rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				int user_id = rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");



				UserMessage message = new UserMessage();
				message.setId(id);
				message.setTitle(title);
				message.setCategory(category);
				message.setText(text);
				message.setInsert_date(insertDate);
				message.setUser_id(user_id);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void deleteUserMessages(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, message.getId());

			ps.executeUpdate();


		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


}

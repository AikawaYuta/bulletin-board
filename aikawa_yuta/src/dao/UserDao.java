package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;


//新規登録
public class UserDao {
	public void insert (Connection connection, User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users( ");
			sql.append("name");
			sql.append(",login_id");
			sql.append(",password");
			sql.append(",branch_id");
			sql.append(",section_id");
			sql.append(",is_deleted");
			sql.append(")VALUES(");
			sql.append("?");//name
			sql.append(",?");//login_id
			sql.append(",?");//password
			sql.append(",?");//branch_id
			sql.append(",?");//section_id
			sql.append(",?");//is_deleted
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getName());
			ps.setString(2, user.getLogin_id());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getSection_id());
			ps.setInt(6, user.getIs_deleted());

			ps.executeUpdate();
		} catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//ログイン情報の取得
	public User getUser(Connection connection, String login_id, String password){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_deleted ='0'";
			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if(2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally{
			close(ps);
		}


	} private List<User> toUserList(ResultSet rs) throws SQLException{
		List<User> userList = new ArrayList<User>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String name =rs.getString("name");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				int branch_id =rs.getInt("branch_id");
				int section_id = rs.getInt("section_id");
				int is_deleted = rs.getInt("is_deleted");


				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setLogin_id(login_id);
				user.setBranch_id(branch_id);
				user.setSection_id(section_id);
				user.setIs_deleted(is_deleted);

				userList.add(user);

			}
				return userList;
		} finally{
			close(rs);
		}
	}

//全ユーザー情報の取得
	public List<User> users(Connection connection){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users ORDER BY branch_id  ";
			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<User> usersList = toUserList(rs);
			return usersList;

		}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}


//編集対象のユーザー情報取得
	public User editUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

//ユーザー情報編集
	public void update (Connection connection,User user, int id,String password){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET  ");
			sql.append("name = ?");
			sql.append(",login_id = ?");
			sql.append(",branch_id = ?");
			sql.append(",section_id = ?");
			if (password != null){
			sql.append(",password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getName());
			ps.setString(2, user.getLogin_id());

			ps.setInt(3, user.getBranch_id());
			ps.setInt(4, user.getSection_id());
			if(password == null){
				ps.setInt(5, id);
			} else {
				ps.setString(5, user.getPassword());
				ps.setInt(6, id);
			}

			ps.executeUpdate();
		} catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
//停止復活
	public void updateUser(Connection connection,User user, int id){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIs_deleted());
			ps.setInt(2, user.getId());

			ps.executeUpdate();
		}	catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	//ログインIDの重複確認
		public User getLogin_id(Connection connection, String login_id) {

			PreparedStatement ps = null;
			try {
				String sql = "SELECT * FROM users WHERE login_id = ?";

				ps = connection.prepareStatement(sql);
				ps.setString(1, login_id);

				ResultSet rs = ps.executeQuery();
				List<User> userList = toUserList(rs);
				if (userList.isEmpty() == true) {
					return null;
				} else {
					return userList.get(0);
				}

			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

}

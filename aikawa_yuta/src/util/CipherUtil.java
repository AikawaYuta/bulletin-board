package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

/*
 * 暗号化ユーティル
 */

public class CipherUtil {

	/*
	 * SHA-256 で暗号化　バイト配列をBase64でエンコーディング
	 *
	 * @param target
	 *
	 * @return
	 *
	 */
	public static String encrypt(String target){
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(target.getBytes());
			return Base64.encodeBase64URLSafeString(md.digest());
		} catch (NoSuchAlgorithmException e){
			throw new RuntimeException(e);
		}

	}

}

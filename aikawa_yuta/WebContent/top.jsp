<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix= "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<script type="text/javascript">

<!--
function check(){
	if(window.confirm('メッセージを削除してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}
// -->
<!--
function check2(){
	if(window.confirm('コメントを削除してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}
// -->
</script>
<div class="main-contents">
<p><font size= 10pt>Home</font></p>
<a href="message"style="display:inline;font-size:15pt;font-weight:bold;">新規投稿画面</a>
<c:if test="${loginUser.section_id == '1' }">
	<a href="user_controll"style="display:inline;font-size:15pt;font-weight:bold;">ユーザー管理画面</a>
</c:if>
<a href="logout"style="display:inline;font-size:15pt;font-weight:bold;">ログアウト</a>

<div class="header">
	<c:if test="${ not empty errorMessages}">
		<div class ="errorMessages">
			<ul>
				<c:forEach items ="${errorMessages}" var = "message">
					<li><c:out value ="${message }" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope = "session"/>
	</c:if>




</div>
<form action="./" method="get">
<br />
開始日 : <input type = "date"  name="startDay" value="${start}" id="start"/>
～終了日: <input type = "date" name="endDay" value="${end}" id="end"/>
  / カテゴリー : <input name="category" value="${cateGory}" id="category"/> <input type="submit" value="絞り込む">
</form>
<div align="right">
<a href="./"style="font-size:12pt;font-weight:bold;">検索条件をリセット</a><br></br>
</div>
<div class="messages">
	<c:forEach items="${messages}" var="message">
		<div class="message">
			<div class="account-name">
				タイトル：<span class="title"><c:out value="${message.title}" /></span><pre></pre>
				カテゴリー：<span class="category"><c:out value="${message.category}" /></span><pre></pre>
				<c:forEach var="text" items = "${fn:split(message.text, '
				')}">
    			<div>${text}</div>
				</c:forEach>

				<c:forEach items="${users }" var="user">
					<div class="user">
					<c:if test="${user.id == message.user_id}">
					投稿者:<c:out value="${user.name}" /></c:if>
					</div>
				</c:forEach>

			</div>
			<div class="date"><fmt:formatDate value="${message.insert_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			<div class="login_user">

			<c:if test="${message.user_id == loginUser.id }">
			<form action="deletemessage" method="post"onSubmit="return check()">
				<input type="hidden" name="message_id" value="${message.id }">
				<input type="submit" value="投稿の削除">
			</form></c:if>
			</div>


			<c:forEach items="${comments }" var="comment">
				<div class="comment">
				<c:if test="${ message.id == comment.message_id }">
					コメント内容：<c:forEach var="comment_text" items = "${fn:split(comment.text, '
				')}">
    			<div>${comment_text}</div>
			</c:forEach>
			<c:forEach items="${users }" var="user">
				<div class="user">
				<c:if test="${user.id == comment.user_id}">
				投稿者:<c:out value="${user.name}" /></c:if>
				</div>
			</c:forEach>
			日時:<span class="date"><fmt:formatDate value="${comment.insert_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span>


					<c:if test="${comment.user_id == loginUser.id }">
					<form action="deletecomment" method="post"onSubmit="return check2()">
					<input type="hidden" name="comment_id" value="${comment.id }">
					<input type="submit" value="コメントの削除">
					</form></c:if>

				</c:if><pre></pre>
				</div>
			</c:forEach>

			<form action="./" method="post">
				<input type="hidden" name="message_id" value="${message.id }">
				コメント（500文字まで）：<textarea name="comment_text" cols="50" rows="5" class="comment_text"></textarea>
				<input type="submit" value="投稿">
			</form>
		</div>


	</c:forEach>
</div>

</div>
<pre></pre>
<div class="copyright">Copyright(c)Yuta Aikawa</div>
</body><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored ="false"%>
<%@taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="ユーザー新規登録"/></title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">
<p><font size="13">New User</font></p>
<c:if test="${not empty errorMessages}">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages }" var="messages">
				<li><c:out value="${messages}"></c:out>
			</c:forEach>
		</ul>
	</div>
<c:remove var="errorMessages" scope = "session"/>
</c:if>

<div class ="signup">
<form action="signup" method="post"><br />
	<label for ="login_id"><c:out value="ログインID"/></label>
	<input name ="login_id" id = "login_id" value = "${signUpUser.login_id}"/>

	<label for ="name"><c:out value="名前"/></label>
	<input name ="name" id ="name" value = "${signUpUser.name}" /><br/>

	<label for="password"><c:out value="パスワード"/></label>
	<input name ="password" type="password" id ="password" />

	<label for="secondPassword"><c:out value="確認用パスワード"/> </label>
	<input name ="secondPassword" type="password" id ="secondPassword" /><br/>

	<c:out value="支店"/>
	<select name="branch_id" id="branch_id">
		<option value = 0><c:out value="選択してください" /></option>
		<c:forEach items="${branches}" var="branch" >
			<c:if test ="${signUpUser.branch_id == branch.id}">
				<option value="${branch.id}"selected><c:out value="${branch.name}" /></option>
			</c:if>
			<c:if test ="${signUpUser.branch_id != branch.id}">
				<option value="${branch.id}"><c:out value="${branch.name}" /></option>
			</c:if>
    	</c:forEach>
	</select><br/>

	<c:out value="部署"/>
	<select name="section_id" id="section_id">
		<option value = 0><c:out value="選択してください" /></option>
    	<c:forEach items="${sections}" var="section" >
   			<c:if test ="${signUpUser.section_id == section.id}">
	    		<option value="${section.id}"selected><c:out value="${section.name}" /></option>
	    	</c:if>
	    	<c:if test ="${signUpUser.section_id != section.id}">
				<option value="${section.id}"><c:out value="${section.name}" /></option>
			</c:if>
	    </c:forEach>
	</select><br/>

	<input type ="submit" value ="登録"/><br/>

</form>

</div>
<pre></pre>
<a href="user_controll"><c:out value="戻る"/></a><pre></pre>
<div class="copyright">Copyright(c)Yuta Aikawa</div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix= "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<p><font size="13">New Message</font></p>
<c:if test="${not empty errorMessages}">
	<div class ="errorMessages">
		<ul>
			<c:forEach items ="${errorMessages}" var = "messages">
				<c:out value ="${messages}" ></c:out><br>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope = "session"/>
</c:if>
<div class="message">
	<form action="message" method="post">

		タイトル：<input name="title" class="title" value = "${messages.title }">（30文字まで)<pre></pre>
		カテゴリー：<input name="category"  class="category"value = "${messages.category }">（10文字まで)<pre></pre>
		本文：<textarea name="text" cols="70" rows="10" class="text" >${messages.text }</textarea>（1000文字まで）<pre></pre>
		<br />
		<input type="submit" value="投稿">
	</form>
</div>
<pre></pre>
<a href="./">戻る</a>


</body>
</html>
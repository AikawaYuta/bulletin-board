<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>${editUser.name}の設定</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">
<p><font size="13">Edit user</font></p>
<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
<c:remove var="errorMessages" scope="session"/>
</c:if>
<div class = "edit_user">
<form action="edit_user" method="post">

	<input type= hidden value="${editUser.id}" name="id"/><br />

	<label for="name">名前</label>
	<input name="name" value="${editUser.name}" style ="15pt"/><br />

	<label for="login_id">ログインID</label>
	<input name="login_id" value="${editUser.login_id}" /><br />

	<label for="password">パスワード</label>
	<input name="password" type="password" id="password"/> <br />

	<label for="secondPassword">確認用パスワード </label>
	<input name ="secondPassword" type="password" id ="secondPassword" /><br/>

	<c:if test="${editUser.id == loginUser.id }">
	<input type= hidden value="${editUser.branch_id}" name="branch_id"/>
	<input type= hidden value="${editUser.section_id}" name="section_id"/>
	</c:if>




	<c:if test="${editUser.id != loginUser.id }">
	支店<select name="branch_id" id="branch_id">
			<c:forEach items="${branches}" var="branch" >
				<c:if test ="${editUser.branch_id == branch.id}">
					<option value="${branch.id}" selected><c:out value="${branch.name}" /></option>
				</c:if>
				<c:if test ="${editUser.branch_id != branch.id}">
					<option value="${branch.id}"><c:out value="${branch.name}" /></option>
				</c:if>
	    	</c:forEach>
	</select><br/>
	</c:if>


	<c:if test="${editUser.id !=loginUser.id }">
	部署<select name="section_id" id="section_id">
			<c:forEach items="${sections}" var="section" >
				<c:if test ="${editUser.section_id == section.id}">
		    		<option value="${section.id}" selected><c:out value="${section.name}" /></option>
	    		</c:if>
	    		<c:if test ="${editUser.section_id != section.id}">
					<option value="${section.id}"><c:out value="${section.name}" /></option>
				</c:if>
		    </c:forEach>
	</select><br/>
	</c:if>


	<input type="submit" value="登録" /> <br />
</form>
</div>
<pre></pre>

<a href="user_controll">戻る</a><pre></pre>
<div class="copyright">Copyright(c)Yuta Aikawa</div>
</div>
</body>
</html>

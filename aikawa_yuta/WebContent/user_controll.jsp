<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix= "c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

<script type="text/javascript">
<!--
function check(){
	if(window.confirm('変更してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}

}
// -->
</script>
<p><font size="13">All Users</font></p>
<c:if test="${ not empty errorMessages}">
	<div class ="errorMessages">
		<ul>
			<c:forEach items ="${errorMessages}" var = "message">
				<li><c:out value ="${message }" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope = "session"/>
</c:if>



<a href="signup"style="display:inline; font-size:15pt; font-weight:bold;">新規登録画面</a>
<a href="./"style="display:inline; font-size:15pt;font-weight:bold;">ホーム</a>

<div class="user_controll">
	<c:forEach items="${users}" var="user">
		<div class="user_controll">
			<div class="user-name">
				<div class="login_id"><c:out value="ログインID：${user.login_id}" /></div>
				<div class="name"><c:out value="名前：${user.name}" /></div>
			</div>
			<div class="branch">
				<c:forEach items="${branches}" var="branch" >
					<c:if test="${user.branch_id == branch.id}">
					<c:out value="支店名："/><c:out value="${branch.name}" /></c:if>
				</c:forEach></div>
			<div class="section">
				<c:forEach items="${sections}" var="section" >
					<c:if test="${user.section_id == section.id}">
					<c:out value="部門名："/><c:out value="${section.name}" /></c:if>
				</c:forEach></div>
			<form action="edit_user"><br /><input type="hidden" name="id" value="${user.id}" />
			 	<input type="submit" value="編集">
			</form>

			<c:if test="${user.id !=loginUser.id }">
			<form action="user_controll" method="post"onSubmit="return check()">
				<c:if test="${user.is_deleted == 0 }"><input type="hidden" name="is_deleted" value = 1>
					<input type="submit" value="停止"></c:if>
				<c:if test="${user.is_deleted == 1 }"><input type="hidden" name="is_deleted" value = 0>
					<input type="submit" value="復活"></c:if>
			<input type="hidden" name="id" value="${user.id }">
			</form>
			</c:if>
			<br>
		</div>

	</c:forEach>
</div>
<a href="./"style="font-size:15pt;font-weight:bold;">ホーム</a>
<div class="copyright">Copyright(c)Yuta Aikawa</div>
</body>
</html>